module vISAcore(clk,thirtytwobitinout);
	input logic [0:0] clk;
	inout logic [31:0] thirtytwobitinout;
	logic [95:0] internalstate;						// register internalstate
	initial internalstate[95:64] = 32'b00000000000000000000000000000000;	// data a-memory-address-bits
	initial internalstate[63:32] = 32'b00000000000000000000000000000000;	// data b-memory-address-bits
	initial internalstate[31:0] = 32'b00000000000000000000000000000000;	// data instruction-pointer-memory-address-bits
	always @(posedge clk) begin
		thirtytwobitinout[31:0] = internalstate[31:0];
		internalstate[95:64] <= thirtytwobitinout[31:0];
		internalstate[63:32] <=	thirtytwobitinout[31:0];
		internalstate[31:0] <=	thirtytwobitinout[31:0];
		thirtytwobitinout[31:0] = internalstate[95:64];
		thirtytwobitinout[31:0] = internalstate[63:32];
	end
endmodule
//
// Offline Documentation: (copied from https://www.latticesemi.com/-/media/LatticeSemi/Documents/ApplicationNotes/EH/FPGA-TN-02039-1-7-ECP5-and-ECP5-5G-sysCONFIG.ashx?document_id=50462)
//
//					What is Necessary and Performant?
//
// 1.  Enter the Slave Parallel Mode, so that any 8-Bits of the FPGA can be Re-Configured at 100-Mhz.
//
// 2.  Enable Transparent Mode, so that FPGA Re-Configuration can still occur while the FPGA is in User-Mode, but
//     don't allow the FPGA to enter User Mode just yet, keep it in Slave Parallel Mode for now.
//
// 3.  Initialize all of the Static Partial Bitstreams.
//
// 4.  Initialize as many Embedded Block Ram Values during Boot for all of the Static Partial Bitstreams as possible
//     in order to maximize performance.
//
// 5.  Right before a Small Part of the FPGA is overwritten with the First Dynamic Partial Bitstream, the DDR3 Memory
//     Controller will send a few Bits. The vISAcore will then subtract the New Value with the Current Value by
//     Reading the Current Value's Embedded Block Ram Memory Address and storing the result into the Current Value's
//     Embedded Block Ram Memory Address. As the last step, the Output of the vISAcore Reading the Current Value's
//     Embedded Block Ram Memory Address is used to either Downclock or Overclock the FPGA by a few tenths to a few
//     hundredths of a Percent. The Current Value is an Signed Value. (Right now, I don't know how many Bits to
//     assign to the Current and New Values, but I will determine the number after doing some testing.)
//
// 6.  Initialize the first Dynamic Partial Bitstream.
//
// 7.  Initialize as few Embedded Block Ram Values for all of the Dynamic Partial Bitstreams during Dynamic Partial
//     Reconfiguration as possible in order to maximize performance.
//
// 8.  Have all of the Static Partial Bitstreams and all of the Dynamic Partial Bitstreams populate and use as many
//     Embedded Block Rams at any moment as possible, in order to maximize performance.
//
// 9.  Right before a Small Part of the FPGA is overwritten with a New Dynamic Partial Bitstream, the DDR3 Memory
//     Controller will send a few Bits. The vISAcore will then subtract the New Value with the Current Value by
//     Reading the Current Value's Embedded Block Ram Memory Address and storing the result into the Current Value's
//     Embedded Block Ram Memory Address. As the last step, the Output of the vISAcore Reading the Current Value's
//     Embedded Block Ram Memory Address is used to either Downclock or Overclock the FPGA by a few tenths to a few
//     hundredths of a Percent. The Current Value is an Signed Value. (Right now, I don't know how many bits to
//     assign to the Current and New Values, but I will determine the number after doing some testing.)
//
// 10. Before the vISAcore Overwrites a Small Region of a Dynamic Partial Bitstream, the Small Region's Clock Enable
//     Input Pin must be Disconnected from its Current Dynamic Partial Bitstream, so that the Current Dynamic Partial
//     Bitstream knows that it just lost a small part of its Dynamic Partial Bitstream to a New Dynamic Partial
//     Bitstream, so that it must make do and continue executing more slowly without it, and, so that the Small
//     Region can be safely Re-Configured while Disconnected from any Dynamic Partial Bitstreams, so that it does not
//     cause any Logical Glitches before its Clock Enable Input Pin Connects to a New Dynamic Partial Bitstream.
//
// 11. After the Small Region has had its Clock Enable Input Pin Disconnected, the Small Region can then be
//     Dynamically Reconfigured to contain a small part of a New Dynamic Partial Bitstream that it intends on
//     Connecting its Clock Enable Input Pin to.
//
// 12. After the Small Region has been Dynamically Reconfigured to contain a small part of a Dynamic Partial
//     Bitstream, the Small Region's Clock Enable Input Pin will now be Connected to the New Dynamic Partial
//     Bitstream, so that the New Dynamic Partial Bitstreams knows that it just gained a small part of its Dynamic
//     Partial Bitstream, and can now put it to use.
//
// 13. Now, Repeat Steps 7 through 12, until the vISAscheduler's two Interrupt Input Pins are set to 2'b1X. When the
//     vISAscheduler's leftmost Interrupt Input Pin is set to 1, the vISAscheduler stops ignoring the rightmost
//     Interrupt Input Pin and actually Processes it. There are four kinds of Interrupts:
//
//	1. "I need you, the vISAscheduler, to send this Keyboard / Mouse Interrupt to the Task that the User has
//	Currently Selected."
//
//	2. "I need you, the vISAscheduler, to begin Outputting Debugging Information about the Process that I was
//      communicating with to Non-Volatile Memory, and tell the vISAos to Prompt the Super-User to choose an Debugger
//	to work on the Debugging Information with, including the choice of not select any Debugger(s), the choice of
//	allowing the Super-User to choose what Bug-Reporting Software to file with, and the choice of to just
//	allowing the Super-User to ignore the problem and restart the Task in question."
//
//	3. "I need you to Enable my specific Hardware Driver as an vISAkernel Kernel Module, because I am a hunk of
//	silicon that needs to be able to communicate with other hunks of silicon."
//
//	4. "I need you to safely Poweroff / Restart vISAos."
//
//     (Not only can the vISAscheduler enable a specific Hardware Driver vISAkernel Kernel Module, the vISAscheduler
//     can also disable a specific Hardware Driver vISAkernel Kernel Module, by checking whether the Hardware in
//     question's Keep-Alive Output Pin has been set to 1'b0. If it has, then the vISAscheduler wakes up the 'Driver
//     Loader / Unloader' Process and gives it the time to unload the specific Hardware Driver vISAkernel Kernel
//     Module. (Only the Process of Unloading a Hardware Driver vISAkernel Kernel Module is allowed to violate the
//     Behavior of 'Earliest Deadline First', but that's an necessary exception that must be tolerated, because the
//     vISAos MUST go out of its way to prevent any Bitstreams from failing to communicate with an otherwise
//     non-physically present Hardware Driver and inevitably causing Memory Corruption. After all this, the Hardware
//     Driver in question's Keep-Alive Output Pin is suddenly unbound and allowed to be used by any Dynamic Partial
//     Bitstream that wants to.
//
// 14. How does the vISAscheduler work?
//
//     The vISAscheduler, is an Earliest Deadline First Scheduler that works like this:
//	1. After a Process is created by the vISAinit Daemon, the newly created Process can choose to either give,
//	   or not give, a Deadline to the vISAscheduler. If it chooses to not give a Deadline, then the vISAscheduler
//	   assigns it the lowest Scheduling Priority. If it chooses to give a Deadline to the vISAscheduler, and if
//	   its Deadline Value happens to be the closer to the Real Time Clock's Value than all other Deadline Values,
//	   then the vISAscheduler assigns it the highest Scheduling Prioritity, and allows it to go first. If the
//	   newly picked Process Voluntarily Yields before it meets its Deadline, then its Deadline is shortened, and
//	   the vISAscheduler allows it to retain or increase its Scheduling Priority. Otherwise, if the newly picked
//	   Process happens to Voluntarily Yield exactly one Clock Cycle before it meets its Deadline, then the
//	   vISAscheduler does not alter its Deadline, and the vISAscheduler allows the newly picked Process to retain
//	   its Scheduling Priority. However, if the newly picked Process happens to meet its Deadline before
//	   Voluntarily Yielding Execution, then the vISAscheduler assigns it the lowest Scheduling Priority.
//
//						What needs to be done?
//
//	1. Enter Slave Parallel Configuration Mode, and load a BBJ Soft-Core, a Keyboard Interrupt Controller, and a
//	few Embedded Block Ram Values. (Don't worry, the Keyboard Interrupt Controller will include Debouncing
//	Logic.)
//	2. Enter User-Mode.
//	3. The BBJ Soft-Core will now load an Multi-Boot Loader called "flossBOOT", which stands for the Free, Libre,
//	   and Open Source Bootloader that is written in Rust and runs on the Bit-Bit-Jump Softcore and uses the
//	   Keyboard Interrupt Controller.
//
//		* If the User does not press any Keyboard Keys, flossBOOT will then wait exactly 3 seconds before
//		  booting the default option off of a Partition that is stored on a Non-Volatile Storage Device.
//
//		  (The 3 seconds default value can be configured by pressing F1 to execute flossUEFI, selecting the
//		  "flossBOOT Configuration" Menu, and changing the flossBOOT Configuration Variable called "AFK Boot
//		  Delay" to whatever unsigned byte value you want, or you can even set it to be infinite by changing
//		  the "Automatic Boot" flossBOOT Configuration Variable to "0". (It is a single Bit value.))
//
//		* During the AFK Boot Delay, if the User held down or pressed the Enter Key, flossBOOT will then skip
//		  waiting exactly one second and immediately Boot the Default Boot Partition that is stored on a
//		  Non-Volatile Storage Device.
//
//		* During the AFK Boot Delay, if the User held down or pressed / presses the Up or Down Arrow Keys,
//		  flossBOOT will then stop automatically Booting from any Partitions on any Non-Volatile Storage
//		  Devices, and it will simply change the currently selected Boot Partition to the one that is above
//		  or below the previously selected Boot Partition. After the Up and / or Down Arrow Key(s) have been
//		  pressed, the Enter Key can be pressed at any time to Boot the Currently Selected Partition that is
//		  stored on a Non-Volatile Storage Device.
//
//		* During the AFK Boot Delay, or after Automatic Booting has been temporarily disabled, if the User
//		  has held down or pressed the F1 Key, the BBJ Soft-Core then begins executing flossUEFI, an Free,
//		  Libre, and Open Source User Extensible Firmware Interface that is written in Rust and compiled for
//		  the Bit-Bit-Jump Soft-Core, and intended to Boot FPGA Operating Systems only on Field Programmable
//		  Gate Arrays that are Compiled and / or Synthesized with entirely Free, Libre, and Open Source Field
//		  Programmable Gate Array Toolchains. (The User can press the F2 Key at any time to exit flossUEFI
//		  and get back into flossBOOT.)
//
//		* To be continued...
//
// 1.  Boot the FPGA using a Primary Model.
// 2.  Load a small amount of Logic, like the vISAcore, a custom DDR3 Memory Controller, a Quad SPI Flash Memory
//     Controller, etc.
// 3.  Set SLAVE_PARALLEL_PORT to ENABLE to retain all of the Slave Parallel Mode I/O Pins in User-Mode.
// 4.  Set DSP[0] to 1'b1 to enable "Transparent Mode", so that the FPGA can be Reconfigured during execution.
// 5.  Keep reconfiguring until a reset pin is pulled. (So that it can safely shut down.) (A capacitor that can store
//     a few seconds of electricity at full load is recommended for safe shutdown.)
//
//	* Using the Slave Parallel Port to overwrite 8-Bits of the FPGA at a time. The following Pins are used:
//		1. D[7:0] Input (Data)
//		2. CCLK[0] Input (FPGA's 100Mhz Clock)
//		3. CSN (Must be set to 1'b0)
//		4. CS1N (Must be set to 1'b0)
//		5. WRITEN (Must be set to 1'b0)
//
//	* To be continued...
//
//					What is Optional, but Desirable?
//
// 1. Multi-FPGA Compatibility.
// 2. Static Partial Bitstream Cyclic-Redundancy-Check Verification.
// 3. Static Partial Bitstream Soft-Error-Detection.
// 4. Static Partial Bitstream Soft-Error-Correction.
// 5. Dynamic Partial Bitstream Cyclic-Redundancy-Check Verification (Does this impact performance heavily? If so, is
// 6. Dynamic Partial Bitstream Soft-Error-Detection		     it worth the tradeoff? If so, then, I wonder if
// 7. Dynamic Partial Bitstream Soft-Error-Correction		     there's a way of doing this without having it
//								     impact performance, either at all, or very
//								     insignificantly...)
// 8. Multi-Boot (supports up to 6 Static Partial Bitstreams, with four or five choices on what to boot. I don't
//    know whether you can choose between the Primary Model and up to four Alternative Models at any time, or whether
//    it only lets you choose one from up to four Alternative Models after the Default Primary Model fails to load.
//    But what I do know for certain, is that IF the Primary Model fails to load, and there are no Alternative Models
//    / all of the available Alternative Models also failed to load, then, and only then, is the Golden Model loaded
//    in order to attempt to solve the problem.)
//
//					What is Optional? But Undesirable?
//
// 1. Serial Configuration Mode.
// 2. Dual Configuration Mode.
// 3. Slave Serial Configuration Mode.
// 4. Serial Peripheral Interface.
// 5. Slave Serial Peripheral Interface.
// 6. Slave Serial Interface Port.
// 7. Master Serial Peripheral Interface.
// 7. Master Serial Peripheral Interface Port.
//
//				Interesting Information found in the Documentation:
//
// 1. "(The FPGA Synchronizes itself on either a 0x8DB3 or 0BAB3 code word.)"
//
// 2. "The User Clock cannot exceed 100Mhz." (By "User Clock", they meant CCLK, which according to the Documentation,
//    is an User-Defined Input Pin that has an maximum Rising and Falling Edge Clock Timing of 5ns and 5ns, which
//    makes it a 100Mhz Clock, and because it's the ONLY 100Mhz Clock that is mentioned at all in the Documentation,
//    they are referring to the User-Defined CCLK Input Pin.)
//
//    ("But vitalmixofnutrients, the Documentation says that CCLK is ACTUALLY limited to 33 Mhz instead of 100 Mhz."
//    No, the Documentation is wrong in this specific instance, they probably meant to say that some other Input Pin
//    is limited to 33 Mhz. CCLK is NOT limited to 33 Mhz, here's why: Right below the 33 Mhz number, it says that
//    CCLK's Minimum High Pulse Width and CCLK's Minimum Low Pulse Width can never be set to any value lower than 5
//    Nanoseconds. Because 5 + 5 = 10, and because 1 Billion divided by 10 is equal to 100 Million, CCLK is actually
//    limited to running at 100 Mhz.) (Remember; Don't blindfully trust the Documentation. Very rarely, some
//    information might happen to become misleading.)
