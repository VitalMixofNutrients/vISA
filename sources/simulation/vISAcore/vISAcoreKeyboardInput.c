#include <stdio.h>
#include <stdlib.h>

#include "vpi_user.h"

#include "acc_user.h"
#include "veriuser.h"

#include "string.h"

handle getkey;
static int getkey_compiletf(char*user_data) {
	(void)(user_data);
	(void)(getkey_compiletf);
	return 0;
};
static int getkey_calltf(char*user_data) {
	(void)(user_data);
	(void)(getkey_calltf);

	char *tmp;
	char *kbinput;
	tmp = malloc(1);
	kbinput = malloc(2);
	kbinput[0] = 'a';
/*	memset(kbinput, 'a', 1); */
	if (scanf("%c",tmp)==1){};
	kbinput[0] = *tmp;
	kbinput[1] = '\0';
/*	memset(kbinput, '\0', 1); */

	acc_initialize();
	getkey = acc_handle_tfarg(1);
	tf_putp (1,*kbinput);

	acc_close();
	free(kbinput);
	free(tmp);
	return 0;
};
void getkey_register(void) {
	s_vpi_systf_data tf_data;
	tf_data.type = vpiSysTask;
	tf_data.tfname = "$getkey";
	tf_data.calltf = getkey_calltf;
	tf_data.compiletf = getkey_compiletf;
	tf_data.sizetf = 0;
	tf_data.user_data = 0;
	vpi_register_systf(&tf_data);
};
void (*vlog_startup_routines[])(void) = {
	getkey_register,
	0
};

/* Let's rewrite the BBJ_verilator_sim_main.cpp File to feature a BBJ Debugger.

#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <ncurses.h>
#include */
