`default_nettype none

module BBJ(clk);

	input wire [0:0] clk;

    reg [268435455:0] memory;

	reg [192:0] BBJreg;
	reg [63:0] BBJipAddr;
	reg [63:0] BBJsrcAddr;
	reg [63:0] BBJdestAddr;

	integer i;

	initial begin
		BBJipAddr[63:0] = 64'b0000000000000000000000000000000000000000000000000000000011000000; // Instruction Pointer Memory Address.
		i = $fopen("../obj_dir/BBJ/initialstate/*", "r");
		while (! $feof(i)) begin
            $fgets(memory, i);
        end
		$fclose(i);
        for (i=0;i<=268435455;i=i+1) begin
            if (memory[i-:8] == 8'b00110000); begin
                    memory[i] = 1'b0;
            end
            if (memory[i-:8] == 8'b00110001); begin
                    memory[i] = 1'b1;
            end
        end
    end
	always @(posedge clk) begin

		BBJreg[191:0] <= memory[BBJipAddr-:192];
		BBJipAddr[63:0] <= BBJreg[191:128];
		BBJsrcAddr[63:0] <= BBJreg[127:64];
		BBJdestAddr[63:0] <= BBJreg[63:0];
		BBJreg[192] <= memory[BBJsrcAddr];
		memory[BBJdestAddr] <= BBJreg[192];

//		memory[BBJreg[63:0]] = memory[BBJreg[127:64]];  This code is commented because it's the
//		BBJreg[191:0] = memory[BBJreg[191:128]-:192];   theoretical minimum SLOC limit for the
//                                                      BBJ ISA to theoretically work.
        case (memory[193])
            1'b1: $finish;
        endcase
//      if (memory[192:192] == 1'b1) begin
//          $finish;
//      end
	end
endmodule
