#include <iostream>
#include <stdlib.h>
#include "VBBJ.h"
#include "verilated.h"
VBBJ *BBJ;
int main(int argc, char **argv) {
	Verilated::commandArgs(argc, argv);
	BBJ = new VBBJ;
	while(!Verilated::gotFinish()) {
		BBJ->clk = 1;
		BBJ->eval();
		BBJ->clk = 0;
		BBJ->eval();
	}
	BBJ->final();
	delete BBJ;
	exit(EXIT_SUCCESS);
}
