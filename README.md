<h2>The vISA Project</h2>

<p>This is the Official Repository of the vISA Project, with vISA standing for "Variable Instruction Set Architecture".</p>

<p>The vISA Project aims to make Variable Instruction Set Architectures running on 100% FLOSS FPGAs the future of General-Purpose Computing.</p>

<h3>Disclaimer:</h3>

<p>The vISA Project is currently an incomplete work in progress, but I will appreciate, consider, and implement good contributions in order to accelerate the development of the vISA Project.</p>

<h3>License:</h3>

<p>By contributing to the vISA Project, you agree that all of your contributions to the vISA Project will be governed under the conditions set by the second version of the <a href="https://gitlab.com/VitalMixofNutrients/vISA/-/blob/vISA/LICENSE">GNU General Public License</a>.</p>

<p>Files in this directory may be available under a different license. Please review any individual file for details.</p>

<h3>Branches:</h3>

<p>The vISA Project will only contain one official git branch: the vISA git branch.</p>

<p>All other git branches of the vISA Project are unofficial and non-permanent, because they are designed to safely test experimental patches against the git clone'd vISA branch for testing, then their pull requests are evaluated and then accepted into the official permanent vISA git branch, or rejected, and the unofficial branches are deleted after the vISA Branch has been rebased to use their commits.</p>

<h3>Mailing Lists:</h3>

<p>The vISA Project will only contain two official and permanent Mailing Lists: The <a href="https://lists.sr.ht/~vitalmixofnutrients/visa-discuss">visa-discuss</a> and <a href="https://lists.sr.ht/~vitalmixofnutrients/submitapullrequesthere">submitapullrequesthere</a> Mailing Lists.</p>

<h3>How does the vISAcore work?</h3>

<p>Using the Reconfiguration Register, which never idles, the partial bitstream(s) that will remain idle for the longest period of time will be overwritten with a partial bitstream / a few partial bitstreams that will remain in use for the longest period of time.</p>

<p>The vISAcore will only execute binaries that have been synthesized with the vISAcompiler tool. To read more about how the vISAcompiler tool works, please read the following <a href="https://gitlab.com/VitalMixofNutrients/vISA/-/blob/vISA/sources/synth/vISAcompiler/README">README</a>.</p>

#<p>Initially, I will design the vISAcore to be as small as possible, so that the partial bitstreams can be as large as possible. If the performance advantages of increasing the size of the vISAcore outweigh the performance disadvantages of decreasing the size of the partial bitstreams, then the vISAcore will expand in size and the partial bitstreams will decrease in size.</p>

<h3>How do I build the vISA Project?</h3>

<p>Disclaimer: At the moment, only the execution of shell scripts is supported. In the future, I will create multiple Makefiles.</p>

<p>WARNING: DO NOT run any of the following shell scripts with superuser privileges. Also, DO NOT run any of the following shell scripts without first inspecting what they do.

<p>The vISA Project is built using the following commands:</p>

<ol><li>First, install the following packages: <code># autoconf bc bison ctags flex g++ gcc gcc-debuginfo gcc-debugsource gcc-c++ git glibc glibc-debuginfo glibc-debugsource gtkwave libgcc libgcc-debuginfo libstdc++ libstdc++-debuginfo make texinfo verilator-4.212* verilator-debuginfo verilator-debugsource yosys</code></li>

</li>If you are running a Redhat-based Linux Distribution, install the following package: <code># dnf install redhat-rpm-config</code></li>
<li>In order to simulate the vISAcore running the vISAcompiler binary that was generated using your code, execute the following shell script: <code>$ cd ./scripts && sh BBJ-simulation.sh ./path/to/vISAcompiler/binary.vISA</code></li></ol>

<p>If you want to quit the simulation at any time, press Ctrl + C.</p>

#<!--<li>Once the vISAcore has been proven to be bug-free, in order to synthesize the vISAcore SystemVerilog source code using Yosys, execute the following shell script: <code>sh ./scripts/synth-for-lattice-ecp5-5g-using-yosys.sh</code></li>-->

<!--<h3>Where can I find the Release Notes for the vISA Project?</h3>

<p>Please see the <a href="https://git.sr.ht/~vitalmixofnutrients/vISA/tree/master/item/RELEASE-NOTES">RELEASE-NOTES file</a> in order to see what has changed with every new release.</p>-->

<h3>What if I just want to use the stable version of the vISA Project?</h3>

<p>If you just want to use the stable version of the vISA Project. then just switch to the vISA branch by typing <code>$ git checkout -t vISA</code>.</p>

<h3>How do I begin contributing to the vISA project?</h3>

<ol><li>In order to do that, <code>$ cd</code> into any directory that you want, for me, that would be <code>/home/vitalmixofnutrients/Projects/vISA/</code>, type <code>$ git clone https://gitlab.com/VitalMixofNutrients/vISA.git your_branch</code>, and press enter. (Replace <code>your_branch<code> with your own branch name.)</li>

<li>Then, you edit and save any file(s) that you want to, make sure that your staging area won't introduce any new bugs, explain your commits as best as you can, and then create the commits for your branch by typing <code>$ git commit -am "My explanation."</code></li>

<li>Then, type <code>$ git push git@gitlab.com:VitalMixofNutrients/vISA.git your_branch</code> and press enter. (Replace your_branch with your own branch name.)</li>

<li>Lastly, create an post about your branch on the <a href="https://lists.sr.ht/~vitalmixofnutrients/submitapullrequesthere">submitapullrequesthere</a> Mailing list in order to attract the attention of other vISA developers and have an chance at being approved into the official and permanent vISA git branch.</li>

<li>While you may discuss your branch with other vISA developers on the <a href="https://lists.sr.ht/~vitalmixofnutrients/submitapullrequesthere">submitapullrequesthere</a> Mailing List, at the end of the day, I, VitalMixofNutrients, will determine whether the vISA branch should be rebased to match your branch.</li></ol>

<h3>Can I suggest an feature request or design rethink for the vISA Project?</h3>

<p>Yes, but only after you make a single post to both the <a href="https://lists.sr.ht/~vitalmixofnutrients/submitapullrequesthere">submitapullrequest</a> and <a href="https://lists.sr.ht/~vitalmixofnutrients/visa-discuss">visa-discuss</a> Mailing Lists, here's how:</p>

<ol><li>In order to post to the <a href="https://lists.sr.ht/~vitalmixofnutrients/submitapullrequesthere">submitapullrequesthere</a> Mailing List, email your post to the following email address: ~vitalmixofnutrients/submitapullrequesthere@lists.sr.ht</li>

<li>In order to post to the <a href="https://lists.sr.ht/~vitalmixofnutrients/visa-discuss">visa-discuss</a> Mailing List, email your post to the following email address: ~vitalmixofnutrients/visa-discuss@lists.sr.ht</li>

<li>Remember to set your Email Client's Composer Mode from HTML or Markdown to "Plain Text" by using <a href="https://useplaintext.email">this guide</a>, because sourcehut requires it for anyone's posts, comments, or replies to be sent and viewed by others on their Mailing Lists.</li></ol>

<h3>Citations:</h3>

<ol><li>S. Nolting, "The NEORV32 Processor", https://github.com/stnolting/neorv32</li></ol>

<h3>Copyrights:</h3>

<ol><li>Copyright (c) 2021, Stephan Nolting. All rights reserved.</li>

<li><p>Verilator is Copyright 2003-2021 by Wilson Snyder. (Report bugs to <a href="https://verilator.org/issues">Verilator Issues</a>.)</p>

<br><p>Verilator is free software: you can redistribute it and/or modify it under the terms of either the GNU Lesser General Public License Version 3 or the Perl Artistic License Version 2.0. See the documentation for more details.</p></li>

<li>GTKWave Wave Viewer is Copyright (C) 1999-2021 Tony Bybell.</li>

<li>yosys -- Yosys Open SYnthesis Suite<br>ISC License

<br>Copyright (C) 2012 - 2020 Claire Wolf <claire@symbioticeda.com></li></ol>

<h3>Trademarks:</h3>

<ol><li>The words "ECP5" and "ECP5-5G" are trademarks of Lattice Semiconductor Corporation.</li></ol>

<h3>Acknowledgements:</h3>

<ol><li>The vISA Project is not affiliated with or endorsed by the Open Source Initiative.

<br>(<a href="https://www.oshwa.org">https://www.oshwa.org</a> / <a href="https://opensource.org">https://opensource.org</a>)</li></ol>
